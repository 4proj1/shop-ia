deploy-dev:
	docker container prune
	docker volume prune
	docker-compose up

migration:
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console make:migration
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console doctrine:migrations:migrate

push:
	docker build . -t ia:0.1
	docker image tag ia:0.1 c-est.party:5000/ia:0.1
	docker push c-est.party:5000/ia:0.1
