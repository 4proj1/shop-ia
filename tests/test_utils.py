from unittest import TestCase

from src.lib.utils import params_to_dict


class Test(TestCase):
    def test_params_to_dict(self):
        params = {"a": "1", "b": "2"}
        if params_to_dict("/?a=1&b=2") != params:
            self.fail()
