from json import dumps

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Text, Date, Boolean, Float, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

BaseSensor = declarative_base()
BaseWeb = declarative_base()

product_category_table = Table('product_category', BaseWeb.metadata,
                               Column('product_id', Integer, ForeignKey('product.id')),
                               Column('category_id', Integer, ForeignKey('category.id'))
                               )

# order_client_product_table = Table('order_client_product', BaseWeb.metadata,
#                                    Column('order_client_id', Integer, ForeignKey('order_client.id')),
#                                    Column('product_id', Integer, ForeignKey('product.id'))
#                                    )


def tiles_entities_to_atomic(tiles):
    json = []

    for tile in tiles:
        json_entity = {}
        for col in ["id","x","y","type"]:
            json_entity[col] = getattr(tile, col)

        for key in tile.__dict__:
            if key != "_sa_instance_state":
                json_entity[key] = tile.__dict__[key]

        json_entity["products"] = []

        for product in tile.products:
            json_entity["products"].append(product_entity_to_atomic(product))

        json.append(json_entity)

    return json


def product_entity_to_atomic(product):
    json = {}

    for col in ["id","name","weight"]:
        json[col] = getattr(product, col)

    for key in product.__dict__:
        if key != "_sa_instance_state":
            json[key] = product.__dict__[key]

    return json


class Tile(BaseSensor):
    __tablename__ = 'tile'

    id = Column(Integer, primary_key=True)
    x = Column(Integer)
    y = Column(Integer)
    type = Column(String(255))
    sensors = relationship("Sensor", back_populates="tile")
    products = relationship("PhysicalProduct", back_populates="tile")


class Sensor(BaseSensor):
    __tablename__ = 'sensor'

    id = Column(Integer, primary_key=True)
    type = Column(String(255))
    way = Column(String(255))
    tile_id = Column(Integer, ForeignKey('tile.id'))
    tile = relationship("Tile", back_populates="sensors")


class PhysicalProduct(BaseSensor):
    __tablename__ = 'product'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    weight = Column(Integer)
    tile_id = Column(Integer, ForeignKey('tile.id'))
    tile = relationship("Tile", back_populates="products")


class User(BaseWeb):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)

    last_name = Column(String(255))
    first_name = Column(String(255))
    full_name = Column(String(255))
    birthday = Column(Date)
    register_at = Column(Date)
    username = Column(String(255))
    sex = Column(Boolean)
    email = Column(String(255))
    phone = Column(String(255))
    address = Column(String(255))
    diet = Column(Text)
    allergies = Column(Text)
    password = Column(String(255))
    is_blocked = Column(Boolean)
    roles = Column(Text)
    # orders = relationship("OrderClient", back_populates="client")


class Product(BaseWeb):
    __tablename__ = 'product'

    id = Column(Integer, primary_key=True)

    name = Column(String(255))
    price = Column(Float)
    Description = Column(String(255))
    # stock = relationship("Stock", back_populates="product")
    categories = relationship(
        "Category",
        secondary=product_category_table,
        back_populates="products")


class OrderClient(BaseWeb):
    __tablename__ = 'order_client'

    id = Column(Integer, primary_key=True)

    order_date = Column(Date)
    client_id = Column(Integer, ForeignKey('user.id'))
    products_list = Column(String(255))


class Category(BaseWeb):
    __tablename__ = 'category'

    id = Column(Integer, primary_key=True)

    name = Column(String(255))
    products = relationship(
        "Product",
        secondary=product_category_table,
        back_populates="categories")


# class Stock(BaseWeb):
#     __tablename__ = 'stock'
#
#     id = Column(Integer, primary_key=True)
#
#     quantity = Column(Integer)
#     product = relationship("Product", back_populates="stock")
