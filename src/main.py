#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socketserver
from http.server import BaseHTTPRequestHandler
from json import dumps

from DAO import get_all_tiles, get_all_products, tiles_entities_to_atomic
from IAs.RecommendationIA import RecommendationIA
from IAs.StoreIA import StoreIA
from IAs.CartIA import CartIA
from IAs.BaseIA import BaseIA
from IAs.BluetoothBeaconsTrackerIA import BluetoothBeaconsTrackerIA
from lib.utils import params_to_dict, dateTimePreLogs
from lib import LogMod

IAs = [BaseIA(), BluetoothBeaconsTrackerIA(), StoreIA(), CartIA(), RecommendationIA()]


class Server(BaseHTTPRequestHandler):
    def do_HEAD(self):
        return

    def do_GET(self):
        url_to_handler = {"/": self.respond, "/getMap": self.get_map}
        url_to_handler[self.path]()

    def do_POST(self):
        print(dateTimePreLogs() + "Request received : " + self.path)
        params = params_to_dict(self.path)
        for ia in IAs:
            ia.process(params)
        self.respond()

    def handle_http(self, status, content_type):
        self.send_response(status)
        self.send_header('Content - type', content_type)
        self.end_headers()
        return bytes('Ok', 'UTF-8')

    def respond(self):
        content = self.handle_http(200, 'text / html')
        self.wfile.write(str(content).encode('utf-8'))

    def get_map(self):
        tiles = get_all_tiles()
        products = get_all_products()

        for tile in tiles:
            for physicalProduct in tile.products:
                for product in products:
                    if product.name == physicalProduct.name:
                        physicalProduct.price = product.price

        self.send_response(200)
        self.send_header('Content - type', 'text / html')
        self.end_headers()
        self.wfile.write(dumps(tiles_entities_to_atomic(tiles)).encode('utf-8'))


if __name__ == '__main__':
    PORT = 8000
    LogMod.setLogger()
    LogMod.addInfo("start")
    Handler = Server

    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print(dateTimePreLogs() + "serving at port", PORT)
        httpd.serve_forever()
