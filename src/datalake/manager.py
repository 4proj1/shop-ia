import os
from cassandra.cluster import Cluster
from cassandra.policies import DCAwareRoundRobinPolicy
from lib.utils import dateTimePreLogs
from DAO import get_tiles_by_type

KEYSPACE = os.getenv('DATA_LAKE_KEY_SPACE', 'ia')

cluster = Cluster([os.getenv('DATA_LAKE_URL', 'localhost')], port=9042,
                  load_balancing_policy=DCAwareRoundRobinPolicy(local_dc='datacenter1'), protocol_version=4)


# input client param : {'x': 132, 'y': 12, 'timestamp': 123532}
def persist_client_movement(id, client):
    # verify input format
    if 'timestamp' not in client or \
            id is None or \
            'x' not in client or \
            'y' not in client:
        print(dateTimePreLogs() + "persist_client_movement - missing information.")
        raise ValueError

    # persist client movement
    session = cluster.connect(KEYSPACE, wait_for_all_pools=True)
    print(dateTimePreLogs() + 'INSERT INTO %s.user_tracking(timestamp, id_user, x, y) VALUES(%s, %s, %s, %s)' % (
        KEYSPACE, client['timestamp'], id, client['x'], client['y']))
    session.execute('INSERT INTO %s.user_tracking(timestamp, id_user, x, y) VALUES(%s, %s, %s, %s)' % (
        KEYSPACE, client['timestamp'], id, client['x'], client['y']))


def get_client_id(x: int, y: int, timestamp: int) -> int:
    session = cluster.connect(KEYSPACE, wait_for_all_pools=True)
    client_id = None
    res = None

    tiles = get_tiles_by_type(tile_type="path")

    query = "SELECT id_user FROM user_tracking WHERE x = {} AND y = {} " \
            "AND timestamp >= {} ALLOW FILTERING"

    for tile in tiles:
        if tile.x == x or tile.y == y:
            res = session.execute(query.format(tile.x, tile.y, timestamp-30000))
            if len(res.current_rows) > 0:
                break

    try:
        client_id = res[len(res.current_rows) - 1][0]
    except:
        pass
    return client_id
