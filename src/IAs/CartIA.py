#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lib import LogMod
from IAs.BaseIA import BaseIA
from datalake.manager import get_client_id
from DAO import get_product_by_coordo, get_web_product_by_name, get_product


class CartIA(BaseIA):
    __slots__ = ["carts", "triggers"]

    # {'x': '1', 'y': '1', 'action': 'take/pose'}
    def __init__(self):
        self.carts = {}
        self.triggers = ["x", "y", "action", "timestamp"]

    def check_params(self, params) -> bool:
        for trigger in self.triggers:
            if trigger not in params:
                LogMod.addWarning(log="cartIA not applicable")
                return False
        return True

    def run(self, data) -> None:
        LogMod.addInfo(log="input params : {}".format(data))
        current_cart = []
        x, y, action, timestamp = [int(data['x']), int(data['y']), data['action'], int(data['timestamp'])]
        sensor_product = get_product(get_product_by_coordo(x=x, y=y)[0])
        product = get_web_product_by_name(sensor_product.name)
        user_id = get_client_id(x=x, y=y, timestamp=timestamp)

        if not isinstance(user_id, int):
            LogMod.addCritical(log=f"Something wrong : Unknown user try to do something")
            return None

        if user_id in self.carts.keys():
            current_cart = self.carts[user_id]
        LogMod.addInfo(log=f"Current cart for {user_id} : {current_cart}")

        LogMod.addInfo(log=f"Product id for tile x:{x} y:{y} : {product.id}")
        LogMod.addInfo(log=f"User {user_id} {action} {product.id}")

        if action == "take":
            current_cart.append(product.id)
        elif action == "pose":
            try:
                current_cart.remove(product.id)
            except ValueError:
                LogMod.addCritical(log=f"Something wrong : user {user_id} remove doesn't existing {product.id} product "
                                       f"from him cart ...")
        else:
            LogMod.addError(log=f"User {user_id} try to ... '{action}' but that wasn't know action")

        LogMod.addInfo(log=f"New current cart for {user_id} : {current_cart}")
        LogMod.addInfo(log=f"Save in process")
        self.carts[user_id] = current_cart
        LogMod.addInfo(log=f"Save finish")

        LogMod.addInfo(log=f"{self.carts}")

    def process(self, params: dict) -> None:
        if self.check_params(params=params):
            LogMod.addInfo("cartIA runnable")
            self.run(data=params)
