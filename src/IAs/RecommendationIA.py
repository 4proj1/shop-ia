import logging
import os

import requests

from DAO import getSensorById, get_near_products, get_products_by_orders
from IAs.BaseIA import BaseIA
from IAs.Ponderations import BUY_FREQUENCY_NOTIFICATION_PICK_NEAR, BUY_FREQUENCY_NOTIFICATION_STAND_NEAR
from datalake.manager import persist_client_movement, get_client_id

# input parameter : {'x': 123, 'y': 123, 'timestamp': 0123465, 'id-user': 1234}
#   OR {'x': '1', 'y': '1', 'action': 'take/pose'}
from lib import LogMod


class RecommendationIA(BaseIA):

    @staticmethod
    def get_action(params):
        move_params = ["x", "y", "timestamp", "id-user"]
        action = True
        for trigger in move_params:
            if trigger not in params:
                action = False

        if action:
            return "move"

        pick_params = ["x", "y", "action", "timestamp"]
        action = True
        for trigger in pick_params:
            if trigger not in params:
                action = False

        if action:
            return "pick"

        return "none"

    @staticmethod
    def get_near_products_with_frequency(x, y, id_user):
        near_products = get_near_products(x, y)
        orders = get_products_by_orders(id_user)
        res = {}

        for product in near_products:
            total = 0
            for order in orders:
                if product.name in order:
                    total += 1
            res[product.name] = total / len(orders)
        return res

    @staticmethod
    def get_names_to_notify(action, frequencies):
        res = []
        for name in frequencies:
            if (action == "pick" and frequencies[name] >= BUY_FREQUENCY_NOTIFICATION_PICK_NEAR) or \
                    (action == "move" and frequencies[name] >= BUY_FREQUENCY_NOTIFICATION_STAND_NEAR):
                res.append(name)
        return res

    def process(self, params):

        action = self.get_action(params)
        if action == "none":
            LogMod.addWarning(log="RecommendationIA not applicable")
            return

        if action == "pick":
            params["id-user"] = get_client_id(x=params["x"], y=params["y"], timestamp=params["timestamp"])

        x, y, id_user, timestamp = (int(params['x']), int(params['y']), params['id-user'], int(params['timestamp']))

        names = self.get_names_to_notify(action, self.get_near_products_with_frequency(x, y, id_user))
        if len(names) == 0:
            return

        notification_url = os.getenv('NOTIFICATION_URL', 'mysql://admin:admin@localhost:3306') + "/addnotification/" + \
            id_user + "--don't forget those : " + \
            str(names)
        LogMod.addInfo(notification_url)
        requests.get(url=notification_url)
