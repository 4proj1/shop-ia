
# A notification will be sent to his phone if a client walk near to product he buy often
BUY_FREQUENCY_NOTIFICATION_STAND_NEAR = 0.75

# A notification will be sent to his phone if a client pick a product near to product he buy often
BUY_FREQUENCY_NOTIFICATION_PICK_NEAR = 0.60

# A notification will be sent to his phone if a client walk near to product he buy often and is solded
BUY_FREQUENCY_NOTIFICATION_STAND_NEAR_SOLDED = 0.70

# A notification will be sent to his phone if a client pick a product near to product he buy often and is solded
BUY_FREQUENCY_NOTIFICATION_PICK_NEAR_SOLDED = 0.55

