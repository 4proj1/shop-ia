import logging
import os

import requests

from DAO import getSensorById
from IAs.BaseIA import BaseIA
from datalake.manager import persist_client_movement

X = "x"

Y = "y"

TIMESTAMP = "timestamp"

ID_USER = "id-user"

tracker = {}


# input parameter : {'x': 123, 'y': 123, 'timestamp': 0123465, 'id-user': 1234}

class BluetoothBeaconsTrackerIA(BaseIA):
    def process(self, params):

        # verify input format
        if ID_USER not in params or \
                TIMESTAMP not in params or \
                Y not in params or \
                X not in params:
            print("BluetoothBeaconsTrackerIA not applicable")
            return

        # compute client position in shop
        x = params[X]
        y = params[Y]

        # build client object
        client = {'x': x, 'y': y, TIMESTAMP: params[TIMESTAMP]}
        tracker[params[ID_USER]] = client

        # persist client movement
        persist_client_movement(params[ID_USER], client)

        notification_url = os.getenv('NOTIFICATION_URL', 'mysql://admin:admin@localhost:3306') +"/addnotification/"+params[ID_USER]+"--"+x+";"+y
        requests.get(url=notification_url)
