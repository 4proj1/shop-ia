#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lib import LogMod
from IAs.BaseIA import BaseIA
from IAs.CartIA import CartIA
from DAO import persist_order


class StoreIA(BaseIA):
    __slots__ = ["cart_ia", "triggers"]

    # {'x': '1', 'y': '1', 'timestamp': '', 'id-user':'1234'}
    def __init__(self):
        self.triggers = ["x", "y", "timestamp", "id-user"]
        self.cart_ia = CartIA()

    def check_params(self, params) -> bool:
        for trigger in self.triggers:
            if trigger not in params:
                LogMod.addWarning(log="storeIA not applicable")
                return False
        return True

    def run(self, data) -> None:
        LogMod.addInfo(log="input params : {}".format(data))
        if (int(data['x']) != -1 and int(data['y']) != -1) and int(data['id-user']) not in self.cart_ia.carts.keys():
            self.cart_ia.carts[int(data['id-user'])] = []
        elif int(data['id-user']) in self.cart_ia.carts.keys() and (int(data['x']) == -1 and int(data['y']) == -1):
            persist_order(client_id=data['id-user'], cart=self.cart_ia.carts.pop(int(data['id-user'])),
                          ts=int(data['timestamp'][:-3]))
            LogMod.addInfo(log=f"Customer {data['id-user']} left the store and his order was registered")

        else:
            LogMod.addWarning(log="storeIA runnable but don't product data")

    def process(self, params: dict) -> None:
        if self.check_params(params=params):
            LogMod.addInfo(log="storeIA runnable")
            self.run(data=params)
