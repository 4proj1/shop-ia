import logging

from DAO import getSensorById
from IAs.BaseIA import BaseIA
from datalake.manager import persist_client_movement

ID_SENSOR = "id-sensor"

TIMESTAMP = "timestamp"

ID_USER = "id-user"

tracker = {}


# input parameter : {'id-sensor': 123, 'timestamp': 0123465, 'id-user': 1234}

class ShopTrackerIA(BaseIA):
    def process(self, params):

        # verify input format
        if ID_USER not in params or \
                TIMESTAMP not in params or \
                ID_SENSOR not in params:
            logging.info("ShopTrackerIA not applicable")
            return

        sensor = getSensorById(params[ID_SENSOR])

        # compute client position in shop
        x = sensor.tile.x
        y = sensor.tile.y

        if sensor.way is None:
            logging.warning("ShopTrackerIA - Sensor way is null.")
            return

        if sensor.way == "top":
            x -= 1
        elif sensor.way == "bottom":
            x += 1
        elif sensor.way == "right":
            y += 1
        elif sensor.way == "left":
            y -= 1

        # build client object
        client = {'x': x, 'y': y, TIMESTAMP: params[TIMESTAMP]}
        tracker[params[ID_USER]] = client

        # persist client movement
        persist_client_movement(params[ID_USER], client)
        logging.info("client movement persisted")
