#!/usr/bin/python
import os

from datetime import datetime


def params_to_dict(params):
    res = {}
    if "=" in params:
        for param in params[2:].split("&"):
            name, value = param.split("=")
            # just for test
            if name == "timestamp":
                res[name] = str(round((datetime.now()).timestamp())) + "000"
            else:
                res[name] = value
    return res


def dateTimePreLogs():
    return str(datetime.today()) + " - "
