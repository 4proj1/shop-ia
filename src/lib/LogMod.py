#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
from logging import StreamHandler

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s', "%d/%m/%Y %H:%M:%S")


def setLogger(lvl="INFO"):
    if lvl == "INFO":
        Handler = StreamHandler()
        Handler.setLevel(logging.getLevelName(lvl))
        Handler.setFormatter(formatter)
        logger.addHandler(Handler)


def addDebug(log):
    logger.debug(log)


def addInfo(log):
    logger.info(log)


def addWarning(log):
    logger.warning(log)


def addError(log):
    logger.error(log)


def addCritical(log):
    logger.critical(log)
