import typing

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, joinedload
from entities import *
import os
import datetime as dt
from datetime import datetime
import json

web_engine = create_engine(os.getenv('WEB_BDD_URL', 'mysql://admin:admin@localhost:3306/dbusers'))
sensor_engine = create_engine(os.getenv('SENSOR_BDD_URL', 'mysql://admin:admin@localhost:3308/test'))


def getSensorById(id):
    Session = sessionmaker(bind=sensor_engine)
    session = Session()
    session.query(Sensor).filter(id)
    session.close()


def get_tile(id):
    Session = sessionmaker(bind=sensor_engine)
    session = Session()
    res = session.query(Tile).get(id)
    session.close()
    return res


def get_all_tiles():
    Session = sessionmaker(bind=sensor_engine)
    session = Session()
    res = session.query(Tile).options(joinedload('products')).all()
    session.close()
    return res


def get_all_products():
    Session = sessionmaker(bind=web_engine)
    session = Session()
    res = session.query(Product).all()
    session.close()
    return res


def get_product_by_coordo(x: int, y: int):
    Session = sessionmaker(bind=sensor_engine)
    session = Session()
    res = session.query(PhysicalProduct.id) \
        .filter(PhysicalProduct.tile_id == session.query(Tile.id)
                .filter(Tile.x == x, Tile.y == y)).all()

    session.close()
    return res[0]


def get_client(id):
    Session = sessionmaker(bind=web_engine)
    session = Session()
    res = session.query(User).get(id)
    session.close()
    return res


def get_product(id):
    Session = sessionmaker(bind=sensor_engine)
    session = Session()
    res = session.query(PhysicalProduct).get(id)
    session.close()
    return res


def get_web_product(id):
    Session = sessionmaker(bind=web_engine)
    session = Session()
    res = session.query(Product).get(id)
    session.close()
    return res


def get_web_product_by_name(name):
    Session = sessionmaker(bind=web_engine)
    session = Session()
    res = session.query(Product) \
        .filter(Product.name == name).first()
    session.close()
    return res


def persist_order(client_id, cart, ts):
    Session = sessionmaker(bind=web_engine)
    session = Session()
    year, month, day = datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d').split('-')

    order_client = OrderClient()
    order_client.client_id = int(client_id)
    order_client.order_date = dt.date(year=int(year), month=int(month), day=int(day))

    res = {}

    for product_id in cart:
        res[product_id] = cart.count(product_id)

    order_client.products_list = str(json.dumps(res))
    session.add(order_client)
    session.flush()
    session.commit()

    session.close()


def get_tiles_by_type(tile_type: str) -> typing.List[Tile]:
    Session = sessionmaker(bind=sensor_engine)
    session = Session()
    res = session.query(Tile) \
        .filter(Tile.type == tile_type).all()
    session.close()
    return res


def get_near_products(x: int, y: int) -> typing.List[PhysicalProduct]:
    Session = sessionmaker(bind=sensor_engine)
    session = Session()
    tiles = session.query(Tile).all()

    res = []
    for tile in tiles:
        if tile.x == x-1 or tile.x == x+1 or tile.y == y+1 or tile.y == y-1:
            for product in tile.products:
                res.append(product)
    session.close()
    return res


def get_products_by_orders(id_user):
    res = []
    Session = sessionmaker(bind=web_engine)
    session = Session()
    orders = session.query(OrderClient) \
        .filter(OrderClient.client_id == id_user).all()

    for i in range(len(orders)):
        products = []
        order = orders[i].products_list.replace("{", "").replace("}", "").replace("\"", "").replace("\\", "").replace(" ", "")
        for order_part in order.split(","):
            products.append(get_web_product(order_part.split(":")[0]).name)
        res.append(products)

    session.close()
    return res
