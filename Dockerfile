FROM python:3.7.0-stretch

COPY ./src /app/project/src

EXPOSE 8000/tcp
EXPOSE 8000/udp

WORKDIR "/app/project"

ENV PYTHONPATH "/app/project"

RUN pip install -r /app/project/src/requirements.txt

CMD ["python", "-u", "src/main.py"]